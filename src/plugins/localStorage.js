export default store => {
  store.subscribe((m,state) => {
    localStorage.setItem('pageState', JSON.stringify(state));
  })
}